const { src, dest, watch, parallel, series } = require(`gulp`);
// const cleanDistg = require(`gulp-clean`);
const sass = require(`gulp-sass`)(require(`sass`));
const concat = require(`gulp-concat`);
const uglify = require(`gulp-uglify-es`).default;
//

function styles() {
  return src(`src/scss/style.scss`)
    .pipe(sass({ outputStyle: `compressed` }))
    .pipe(concat(`style.min.css`))
    .pipe(dest(`dist/css`));
}

function scripts() {
  return src([`src/js/*.js`])
    .pipe(concat(`main.min.js`))
    .pipe(uglify())
    .pipe(dest(`dist/js`));
}

// // function images() {
// //   return src("src/img/*.*").pipe(imagemin()).pipe(dest("dist/img"));
// }

function watching() {
  watch(["src/scss/*.scss"], styles);
  watch(["src/js/*.js"], scripts);
  // watch(["src/img/*.*"], images);
}

// function cleanDist() {
//   return src("dist/**/*").pipe(clean());
// }

exports.styles = styles;
exports.scripts = scripts;
exports.watching = watching;
exports.dev = parallel(styles, scripts, watching);
exports.build = series(styles, scripts);
